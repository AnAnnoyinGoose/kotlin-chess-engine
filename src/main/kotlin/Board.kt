import piece.Piece
import piece.PieceColor
import piece.PieceType
import piece.pieces.*
@Suppress("unused")
enum class Notation {
    A8, B8, C8, D8, E8, F8, G8, H8,
    A7, B7, C7, D7, E7, F7, G7, H7,
    A6, B6, C6, D6, E6, F6, G6, H6,
    A5, B5, C5, D5, E5, F5, G5, H5,
    A4, B4, C4, D4, E4, F4, G4, H4,
    A3, B3, C3, D3, E3, F3, G3, H3,
    A2, B2, C2, D2, E2, F2, G2, H2,
    A1, B1, C1, D1, E1, F1, G1, H1
}

@Suppress("unused")
enum class Files {
    A, B, C, D, E, F, G, H
}

@Suppress("unused")
enum class Ranks {
    RANK_1, RANK_2, RANK_3, RANK_4, RANK_5, RANK_6, RANK_7, RANK_8
}



class Board(fen: String = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1") {
    private val board = Array<Piece>(64) { None() }

    private val turn: PieceColor = PieceColor.WHITE
    private val enPassantTarget: Piece? = null
    private val halfMoveClock: Int = 0
    private val fullMoveCounter: Int = 1

    private val castleK: Array<Boolean> = arrayOf(true, true) // 0 - white, 1 - black
    private val castleQ: Array<Boolean> = arrayOf(true, true) // 0 - white, 1 - black
    internal val castlingAbility = { color: PieceColor -> castleK[color.ordinal] to castleQ[color.ordinal] }

    private val attacked: Set<Notation> = setOf()

    internal val endGame = false

    init {
        validateFen(fen)
        applyFEN(fen)
        printBoard()

    }

    fun getAllWhitePieces(): List<Piece> {
        return board.toList().filter { it.pieceColor == PieceColor.WHITE }
    }

    fun getAllBlackPieces(): List<Piece> {
        return board.toList().filter { it.pieceColor == PieceColor.BLACK }
    }

    fun getAllOf(piece: PieceType, color: PieceColor) : List<Piece> {
        return board.toList().filter { it.pieceColor == color && it.pieceType == piece }
    }

    fun placePiece(piece: Piece, notation: Notation) {
        board[notation.ordinal] = piece
    }

    fun getPiece(notation: Notation): Piece {
        return board[notation.ordinal]
    }

    fun getPiecesInFile(file: Files): List<Piece> {
        val result = mutableListOf<Piece>()
        for (row in 0..7) {
            val piece = board[(row * 8) + file.ordinal]
            if (piece is None) continue
            result.add(piece)
        }
        return result
    }
    fun getPiecesInNeighbourFiles(file: Files): Pair<List<Piece>, List<Piece>> {

        var left = listOf<Piece>()
        var right = listOf<Piece>()
        if (file.ordinal > 0) left = getPiecesInFile(Files.entries[file.ordinal - 1])
        if (file.ordinal < 7) right = getPiecesInFile(Files.entries[file.ordinal + 1])
        return left to right

    }



    fun getPiecesInRank(rank: Ranks): List<Piece> {
        val result = mutableListOf<Piece>()
        for (col in 0..7) {
            val piece = board[(rank.ordinal * 8) + col]
            if (piece is None) continue
            result.add(piece)
        }
        return result
    }

    fun isOccupied(notation: Notation): Boolean {
        return getPiece(notation) != None()
    }

    fun getPieceColor(notation: Notation): PieceColor {
        return getPiece(notation).pieceColor
    }

    fun getPieceType(notation: Notation): PieceType {
        return getPiece(notation).pieceType
    }

    private fun applyFEN(fen: String) {
        val parts: List<String> = fen.split("/", " ")
        parts.subList(0, 8).forEachIndexed { row, part ->
            var col = 0
            part.forEach { char ->
                when (char) {
                    'P' -> placePiece(
                        Pawn(
                            PieceColor.WHITE,
                            Notation.entries[(row * 8) + col],
                            Ranks.entries[row],
                            Files.entries[col]
                        ),
                        Notation.entries[(row * 8) + col]
                    )

                    'N' -> placePiece(
                        Knight(
                            PieceColor.WHITE,
                            Notation.entries[(row * 8) + col],
                            Ranks.entries[row],
                            Files.entries[col]
                        ),
                        Notation.entries[(row * 8) + col]
                    )

                    'B' -> placePiece(
                        Bishop(
                            PieceColor.WHITE,
                            Notation.entries[(row * 8) + col],
                            Ranks.entries[row],
                            Files.entries[col]
                        ),
                        Notation.entries[(row * 8) + col]
                    )

                    'R' -> placePiece(
                        Rook(
                            PieceColor.WHITE,
                            Notation.entries[(row * 8) + col],
                            Ranks.entries[row],
                            Files.entries[col]
                        ),
                        Notation.entries[(row * 8) + col]
                    )

                    'Q' -> placePiece(
                        Queen(
                            PieceColor.WHITE,
                            Notation.entries[(row * 8) + col],
                            Ranks.entries[row],
                            Files.entries[col]
                        ),
                        Notation.entries[(row * 8) + col]
                    )

                    'K' -> placePiece(
                        King(
                            PieceColor.WHITE,
                            Notation.entries[(row * 8) + col],
                            Ranks.entries[row],
                            Files.entries[col]
                        ),
                        Notation.entries[(row * 8) + col]
                    )

                    'p' -> placePiece(
                        Pawn(
                            PieceColor.BLACK,
                            Notation.entries[(row * 8) + col],
                            Ranks.entries[row],
                            Files.entries[col]
                        ),
                        Notation.entries[(row * 8) + col]
                    )

                    'n' -> placePiece(
                        Knight(
                            PieceColor.BLACK,
                            Notation.entries[(row * 8) + col],
                            Ranks.entries[row],
                            Files.entries[col]
                        ),
                        Notation.entries[(row * 8) + col]
                    )

                    'b' -> placePiece(
                        Bishop(
                            PieceColor.BLACK,
                            Notation.entries[(row * 8) + col],
                            Ranks.entries[row],
                            Files.entries[col]
                        ),
                        Notation.entries[(row * 8) + col]
                    )

                    'r' -> placePiece(
                        Rook(
                            PieceColor.BLACK,
                            Notation.entries[(row * 8) + col],
                            Ranks.entries[row],
                            Files.entries[col]
                        ),
                        Notation.entries[(row * 8) + col]
                    )

                    'q' -> placePiece(
                        Queen(
                            PieceColor.BLACK,
                            Notation.entries[(row * 8) + col],
                            Ranks.entries[row],
                            Files.entries[col]
                        ),
                        Notation.entries[(row * 8) + col]
                    )

                    'k' -> placePiece(
                        King(
                            PieceColor.BLACK,
                            Notation.entries[(row * 8) + col],
                            Ranks.entries[row],
                            Files.entries[col]
                        ),
                        Notation.entries[(row * 8) + col]
                    )

                    in '1'..'8' -> {
                        val emptySquares = char.toString().toInt()
                        repeat(emptySquares - 1) {
                            col++
                        }
                    }
                }
                col++
            }
        }
    }
    private fun validateFen(fen: String) {
        if (!FENValidator(fen).isValid()) throw IllegalArgumentException("Invalid FEN")
    }

    private fun printBoard() {
        for (row in 0..7) {
            for (col in 0..7) {
                val notation = Notation.entries[(row * 8) + col]
                print(getPiece(notation).getPieceNotation())
            }
            println()
        }
    }

    private fun isLegalMove(from: Notation, to: Notation): Boolean {
        return (from != to) && (getPieceColor(from) != getPieceColor(to)) && (getPiece(from) != None()) && (!getPiece(from).isPinned())
    }

    fun makeMove(from: Notation, to: Notation) {
        if (!isLegalMove(from, to)) throw IllegalArgumentException("Illegal move")
        val piece = getPiece(from)
        board[to.ordinal] = piece
        board[from.ordinal] = None()
    }

    fun getPawnChains(color: PieceColor): List<List<Pawn>> {
        val connectedPawnChains = mutableListOf<List<Pawn>>()
        val visitedPawns = mutableSetOf<Notation>()

        for (file in Files.entries) {
            for (rank in Ranks.entries) {
                val notation = getNotation(file, rank)
                val piece = getPiece(notation)

                if (piece is Pawn && piece.pieceColor == color && notation !in visitedPawns) {
                    val pawnChain = mutableListOf<Pawn>()
                    findConnectedPawns(notation, color, visitedPawns, pawnChain)
                    connectedPawnChains.add(pawnChain)
                }
            }
        }

        return connectedPawnChains
    }

    private fun findConnectedPawns(
        currentNotation: Notation,
        color: PieceColor,
        visitedPawns: MutableSet<Notation>,
        pawnChain: MutableList<Pawn>
    ) {
        if (currentNotation in visitedPawns) {
            return
        }

        val currentPawn = getPiece(currentNotation) as Pawn
        if (currentPawn.pieceColor != color) {
            return
        }

        visitedPawns.add(currentNotation)
        pawnChain.add(currentPawn)

        for (neighbor in getAdjacentPawns(currentNotation, color)) {
            findConnectedPawns(neighbor, color, visitedPawns, pawnChain)
        }
    }

    private fun getAdjacentPawns(notation: Notation, color: PieceColor): List<Notation> {
        val adjacentPawns = mutableListOf<Notation>()

        val (left, right) = getPiecesInNeighbourFiles(getFile(notation))

        if (left.all { it.rank.ordinal < Ranks.RANK_8.ordinal } && left.isNotEmpty()) {
            adjacentPawns.addAll(left.filter {  it is Pawn && it.pieceColor == color }.map { it.currentLocation })
        }

        if (right.all { it.rank.ordinal > Ranks.RANK_1.ordinal } && right.isNotEmpty()) {
            adjacentPawns.addAll(right.filter {  it is Pawn && it.pieceColor == color }.map { it.currentLocation })
        }



        return adjacentPawns
    }

    private fun getFile(notation: Notation): Files {
        return Files.entries[notation.ordinal % 8]
    }

    private fun getNotation(file: Files, rank: Ranks): Notation {
        return Notation.entries[rank.ordinal * 8 + file.ordinal]
    }
}