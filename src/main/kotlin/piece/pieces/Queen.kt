package piece.pieces

import Files
import Notation
import Ranks
import piece.Piece
import piece.PieceColor
import piece.PieceType

class Queen(
    override val pieceColor: PieceColor = PieceColor.NONE,
    override var currentLocation: Notation,
    override var rank: Ranks,
    override var file: Files
) : Piece {
    override val pieceType: PieceType = PieceType.QUEEN
}