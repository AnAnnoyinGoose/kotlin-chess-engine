package piece.pieces

import Files
import Notation
import Ranks
import piece.Piece
import piece.PieceColor
import piece.PieceType

class None : Piece
{
    override var currentLocation: Notation = Notation.H1
    override var rank: Ranks = Ranks.RANK_1
    override var file: Files = Files.A
    override val pieceType: PieceType = PieceType.NONE
    override val pieceColor: PieceColor = PieceColor.NONE
}
