package piece

enum class PieceType(val score: Double, val char: Char) {
    NONE(0.0, '.'),
    PAWN(1.0, 'P'),
    KNIGHT(3.0, 'N'),
    BISHOP(3.0, 'B'),
    ROOK(5.0, 'R'),
    QUEEN(9.0, 'Q'),
    KING(100.0, 'K');
}