package piece

import Files
import Notation
import Ranks

interface Piece {
    val pieceType: PieceType
    val pieceColor: PieceColor
    var currentLocation: Notation
    var rank: Ranks
    var file: Files
    private var pinned: Boolean
        get() = false
        set(value) {}

    fun getPieceNotation(): Char {
        return if (pieceColor == PieceColor.WHITE) {
            pieceType.char
        } else {
            pieceType.char.lowercaseChar()
        }
    }

    fun isPinned(): Boolean {
        return pinned
    }
}
// Implement pawn, bishop, rook, queen, king

