import piece.Piece
import piece.PieceType

/**
 * @note The values are ripped from Sebastian Lague's Chess Engine - https://github.com/SebLague/Chess-Coding-Adventure/blob/Chess-V2-UCI/Chess-Coding-Adventure/src/Core/Evaluation/PieceSquareTable.cs
 */
object PositionalAdvantage {

    private val pawnPositionAdvantage: List<Double> = listOf(
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
        10.0, 10.0, 20.0, 30.0, 30.0, 20.0, 10.0, 10.0,
        5.0, 5.0, 10.0, 25.0, 25.0, 10.0, 5.0, 5.0,
        0.0, 0.0, 0.0, 20.0, 20.0, 0.0, 0.0, 0.0,
        5.0, -5.0, -10.0, 0.0, 0.0, -10.0, -5.0, 5.0,
        5.0, 10.0, 10.0, -20.0, -20.0, 10.0, 10.0, 5.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
    )
    private val pawnPositionAdvantageEndGame: List<Double> = listOf(
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        80.0, 80.0, 80.0, 80.0, 80.0, 80.0, 80.0, 80.0,
        50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
        30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0,
        20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0,
        10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0,
        10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
    )

    private val knightPositionAdvantage: List<Double> = listOf(
        -50.0, -40.0, -30.0, -30.0, -30.0, -30.0, -40.0, -50.0,
        -40.0, -20.0, 0.0, 0.0, 0.0, 0.0, -20.0, -40.0,
        -30.0, 0.0, 10.0, 15.0, 15.0, 10.0, 0.0, -30.0,
        -30.0, 5.0, 15.0, 20.0, 20.0, 15.0, 5.0, -30.0,
        -30.0, 0.0, 15.0, 20.0, 20.0, 15.0, 0.0, -30.0,
        -30.0, 5.0, 10.0, 15.0, 15.0, 10.0, 5.0, -30.0,
        -40.0, -20.0, 0.0, 5.0, 5.0, 0.0, -20.0, -40.0,
        -50.0, -40.0, -30.0, -30.0, -30.0, -30.0, -40.0, -50.0
    )
    private val bishopPositionAdvantage: List<Double> = listOf(
        -20.0, -10.0, -10.0, -10.0, -10.0, -10.0, -10.0, -20.0,
        -10.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -10.0,
        -10.0, 0.0, 5.0, 10.0, 10.0, 5.0, 0.0, -10.0,
        -10.0, 5.0, 5.0, 10.0, 10.0, 5.0, 5.0, -10.0,
        -10.0, 0.0, 10.0, 10.0, 10.0, 10.0, 0.0, -10.0,
        -10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, -10.0,
        -10.0, 5.0, 0.0, 0.0, 0.0, 0.0, 5.0, -10.0,
        -20.0, -10.0, -10.0, -10.0, -10.0, -10.0, -10.0, -20.0
    )

    private val rookPositionAdvantage: List<Double> = listOf(
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        5.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 5.0,
        -5.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -5.0,
        -5.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -5.0,
        -5.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -5.0,
        -5.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -5.0,
        -5.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -5.0,
        0.0, 0.0, 0.0, 5.0, 5.0, 0.0, 0.0, 0.0
    )

    private val queenPositionAdvantage: List<Double> = listOf(
        -20.0, -10.0, -10.0, -5.0, -5.0, -10.0, -10.0, -20.0,
        -10.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -10.0,
        -10.0, 0.0, 5.0, 5.0, 5.0, 5.0, 0.0, -10.0,
        -5.0, 0.0, 5.0, 5.0, 5.0, 5.0, 0.0, -5.0,
        0.0, 0.0, 5.0, 5.0, 5.0, 5.0, 0.0, -5.0,
        -10.0, 5.0, 5.0, 5.0, 5.0, 5.0, 0.0, -10.0,
        -10.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, -10.0,
        -20.0, -10.0, -10.0, -5.0, -5.0, -10.0, -10.0, -20.0
    )

    private val kingPositionAdvantage: List<Double> = listOf(
        -80.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -80.0, -60.0, -60.0, -60.0, -60.0, -60.0, -60.0, -60.0, -60.0,
        -40.0, -50.0, -50.0, -60.0, -60.0, -50.0, -50.0, -40.0, -30.0, -40.0, -40.0, -50.0, -50.0, -40.0, -40.0, -30.0,
        -20.0, -30.0, -30.0, -40.0, -40.0, -30.0, -30.0, -20.0, -10.0, -20.0, -20.0, -20.0, -20.0, -20.0, -20.0, -10.0,
        20.0, 20.0, -5.0, -5.0, -5.0, -5.0, 20.0, 20.0, 20.0, 30.0, 10.0, 0.0, 0.0, 10.0, 30.0, 20.0
    )

    private val kingPositionalAdvantageEndGame: List<Double> = listOf(
        -20.0, -10.0, -10.0, -10.0, -10.0, -10.0, -10.0, -20.0, -5.0, 0.0, 5.0, 5.0, 5.0, 5.0, 0.0, -5.0,
        -10.0, -5.0, 20.0, 30.0, 30.0, 20.0, -5.0, -10.0, -15.0, -10.0, 35.0, 45.0, 45.0, 35.0, -10.0, -15.0,
        -20.0, -15.0, 30.0, 40.0, 40.0, 30.0, -15.0, -20.0, -25.0, -20.0, 20.0, 25.0, 25.0, 20.0, -20.0, -25.0,
        -30.0, -25.0, 0.0, 0.0, 0.0, 0.0, -25.0, -30.0, -50.0, -30.0, -30.0, -30.0, -30.0, -30.0, -30.0, -50.0
    )

    fun getWhitePA(piece: Piece, endGame: Boolean): Double {
        return when (piece.pieceType) {
            PieceType.PAWN -> if (!endGame) pawnPositionAdvantage[piece.currentLocation.ordinal] else pawnPositionAdvantageEndGame[piece.currentLocation.ordinal]
            PieceType.KNIGHT -> knightPositionAdvantage[piece.currentLocation.ordinal]
            PieceType.BISHOP -> bishopPositionAdvantage[piece.currentLocation.ordinal]
            PieceType.ROOK -> rookPositionAdvantage[piece.currentLocation.ordinal]
            PieceType.QUEEN -> queenPositionAdvantage[piece.currentLocation.ordinal]
            PieceType.KING -> if (!endGame) kingPositionAdvantage[piece.currentLocation.ordinal] else kingPositionalAdvantageEndGame[piece.currentLocation.ordinal]
            PieceType.NONE -> 0.0
        }
    }


    fun getBlackPA(piece: Piece, endGame: Boolean): Double {
        return when (piece.pieceType) {
            PieceType.PAWN -> if (!endGame) pawnPositionAdvantage.reversed()[piece.currentLocation.ordinal] else pawnPositionAdvantageEndGame.reversed()[piece.currentLocation.ordinal]
            PieceType.KNIGHT -> knightPositionAdvantage.reversed()[piece.currentLocation.ordinal]
            PieceType.BISHOP -> bishopPositionAdvantage.reversed()[piece.currentLocation.ordinal]
            PieceType.ROOK -> rookPositionAdvantage.reversed()[piece.currentLocation.ordinal]
            PieceType.QUEEN ->queenPositionAdvantage.reversed()[piece.currentLocation.ordinal]
            PieceType.KING -> if (!endGame) kingPositionAdvantage.reversed()[piece.currentLocation.ordinal] else kingPositionalAdvantageEndGame.reversed()[piece.currentLocation.ordinal]
            PieceType.NONE -> 0.0
        }
    }
}