import piece.Piece
import piece.PieceColor


class Evaluator(private val board: Board) {

    companion object {
        private const val ISOLATED_PAWN_PENALTY = -1.0
        private const val DOUBLED_PAWN_PENALTY = -0.5
    }


    fun evaluate(): Double {
        var score = 0.0
        score += calcPosiotionalAdvantage()
        score += PawnEval(board).calculateTotalPawnScore(PieceColor.WHITE) - PawnEval(board).calculateTotalPawnScore(PieceColor.BLACK)
        return score
    }

    private fun calcPosiotionalAdvantage(): Double {
        var score = 0.0
        val white = board.getAllWhitePieces()
        val black = board.getAllBlackPieces()
        for (piece in white) {
            score += evaluatePieceWhite(piece)
        }
        for (piece in black) {
            score += evaluatePieceBlack(piece)
        }
        return score
    }



/**
    private fun checkIsolation(pawn: Pawn) : Int {
        val neighbours = board.getPiecesInNeighbourFiles(pawn.file)
        val leftNeighbours = neighbours.first.filter { it is Pawn && it.pieceColor == pawn.pieceColor }
        logger.debug("Left neighbours of ${pawn.pieceColor} ${pawn.currentLocation}: ${leftNeighbours.size}")
        val rightNeighbours = neighbours.second.filter { it is Pawn && it.pieceColor == pawn.pieceColor }
        logger.debug("Right neighbours of ${pawn.pieceColor} ${pawn.currentLocation}: ${rightNeighbours.size}")
        if (leftNeighbours.isNotEmpty() || rightNeighbours.isNotEmpty()) {
            return 0
        }

        logger.debug("Pawn at ${pawn.currentLocation} is isolated")
        return 1
    }
    */





    private fun evaluatePieceWhite(piece: Piece): Double {
        return PositionalAdvantage.getWhitePA(piece, board.endGame) / 10 + piece.pieceType.score
    }

    private fun evaluatePieceBlack(piece: Piece): Double {
        return (PositionalAdvantage.getBlackPA(piece, board.endGame) / 10 + piece.pieceType.score) * -1
    }
}