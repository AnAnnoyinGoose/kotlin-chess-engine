class FENValidator(private val fen: String) {
    fun isValid(): Boolean {
        return isValidFEN(fen)
    }
    private fun isValidFEN(fen: String): Boolean {
        if (fen.length > 83 || fen.length < 23) return false // 83 - max length of FEN, 23 - min length of FEN
        val parts = fen.split(" ")

        if (parts.size != 6) {
            return false
        }

        val piecePlacement = parts[0]
        val sideToMove = parts[1]
        val castlingAbility = parts[2]
        val enPassantTarget = parts[3]
        val halfMoveClock = parts[4]
        val fullMoveCounter = parts[5]

        return isValidPiecePlacement(piecePlacement) &&
               isValidSideToMove(sideToMove) &&
               isValidCastlingAbility(castlingAbility) &&
               isValidEnPassantTarget(enPassantTarget) &&
               isValidHalfMoveClock(halfMoveClock) &&
               isValidFullMoveCounter(fullMoveCounter)
    }

    private fun isValidPiecePlacement(piecePlacement: String): Boolean {
        val ranks = piecePlacement.split("/")
        return ranks.size == 8 && ranks.all { isValidRank(it) }
    }

    private fun isValidRank(rank: String): Boolean {
        return rank.matches(Regex("[1-8pkqbnrPKQBNR]*"))
    }

    private fun isValidSideToMove(sideToMove: String): Boolean {
        return sideToMove.matches(Regex("^(w|b)$"))
    }

    private fun isValidCastlingAbility(castlingAbility: String): Boolean {
        return castlingAbility.matches(Regex("^-$|^[KQkq]+$"))
    }

    private fun isValidEnPassantTarget(enPassantTarget: String): Boolean {
        return enPassantTarget.matches(Regex("^(-|[a-h][36])$"))
    }

    private fun isValidHalfMoveClock(halfMoveClock: String): Boolean {
        return halfMoveClock.matches(Regex("^\\d+$"))
    }

    private fun isValidFullMoveCounter(fullMoveCounter: String): Boolean {
        return fullMoveCounter.matches(Regex("^\\d+$"))
    }

}


