import piece.PieceColor
import piece.PieceType
import piece.pieces.Pawn


class PawnEval(private val board: Board) {




    private fun calculatePawnChainScore(chain: List<Pawn>, color: PieceColor): Double {

        var chainScore = chain.size.toDouble()


        chainScore += calculatePawnStructureScore(chain, color)


        chainScore += calculatePawnAdvancementScore(chain, color)


        chainScore += calculateKeySquareOccupancyScore(chain, color)
        return chainScore
    }

    private fun calculatePawnStructureScore(chain: List<Pawn>, color: PieceColor): Double {

        var structureScore = 0.0

        for (i in 1 until chain.size - 1) {
            if (chain[i - 1].file != chain[i].file && chain[i + 1].file != chain[i].file) {

                structureScore -= if (color == PieceColor.WHITE) 0.5 else -0.5
            } else {

                structureScore += if (color == PieceColor.WHITE) 0.2 else -0.2
            }
        }
        println("Pawn Structure Score: $structureScore for $chain of color $color")
        return structureScore
    }

    private fun calculatePawnAdvancementScore(chain: List<Pawn>, color: PieceColor): Double {

        val averageRank = chain.sumOf { it.rank.ordinal } / chain.size.toDouble()
        return (if (color == PieceColor.WHITE) 7 - averageRank else averageRank) * 0.1
    }

    private fun calculateKeySquareOccupancyScore(chain: List<Pawn>, color: PieceColor): Double {

        val keySquares = setOf(Files.D, Files.E)
        var keySquareScore = 0.0

        for (pawn in chain) {
            if (pawn.file in keySquares) {
                keySquareScore += if (color == PieceColor.WHITE) 0.3 else -0.3
            }
        }
        println("Key Square Score: $keySquareScore for $chain of color $color")
        return keySquareScore
    }


    private fun calculateDoubledPawnScore(chain: List<Pawn>, color: PieceColor): Double {

        val doubledPawnFiles = mutableSetOf<Files>()

        for (pawn in chain) {
            if (pawn.file in doubledPawnFiles) {
                println("Doubled Pawn Score for $chain of color $color")
                return if (color == PieceColor.WHITE) -0.5 else 0.5
            } else {
                doubledPawnFiles.add(pawn.file)
            }
        }

        return 0.0
    }

    private fun calculateTripledPawnScore(chain: List<Pawn>, color: PieceColor): Double {

        val tripledPawnFiles = mutableSetOf<Files>()

        for (pawn in chain) {
            if (pawn.file in tripledPawnFiles) {
                println("Tripled Pawn Score for $chain of color $color")
                return if (color == PieceColor.WHITE) -1.0 else 1.0
            } else {
                tripledPawnFiles.add(pawn.file)
            }
        }

        return 0.0
    }

    private fun calculateBackwardPawnScore(chain: List<Pawn>, color: PieceColor): Double {

        var backwardPawnScore = 0.0

        for (i in 1 until chain.size - 1) {
            if (chain[i - 1].file == chain[i].file || chain[i + 1].file == chain[i].file) {

                if (chain[i - 1].rank.ordinal <= chain[i].rank.ordinal && chain[i + 1].rank.ordinal <= chain[i].rank.ordinal) continue
                backwardPawnScore += if (color == PieceColor.WHITE) -0.5 else 0.5
            }
        }
        println("Backward Pawn Score: $backwardPawnScore for $chain of color $color")
        return backwardPawnScore
    }

    private fun calculateSentryPawnScore(chain: List<Pawn>, color: PieceColor): Double {

        var sentryPawnScore = 0.0

        for (i in 1 until chain.size - 1) {
            if (chain[i - 1].file == chain[i].file || chain[i + 1].file == chain[i].file) {

                if (chain[i - 1].rank.ordinal < chain[i].rank.ordinal && chain[i + 1].rank.ordinal < chain[i].rank.ordinal) {

                    sentryPawnScore += if (color == PieceColor.WHITE) 0.5 else -0.5
                }
            }
        }
        println("Sentry Pawn Score: $sentryPawnScore for $chain of color $color")
        return sentryPawnScore
    }

    private fun calculatePassedPawnScore(chain: List<Pawn>, color: PieceColor): Double {

        var passedPawnScore = 0.0

        for (pawn in chain) {
            val opponentColor = if (pawn.pieceColor == PieceColor.WHITE) PieceColor.BLACK else PieceColor.WHITE
            val opponentPawns = board.getAllOf(PieceType.PAWN, opponentColor)

            if (opponentPawns.none { it.file == pawn.file || it.file.ordinal == pawn.file.ordinal - 1 || it.file.ordinal == pawn.file.ordinal + 1 }) {

                passedPawnScore += if (color == PieceColor.WHITE) 3.0 else -3.0
            }
        }
        println("Passed Pawn Score: $passedPawnScore for $chain of color $color")
        return passedPawnScore
    }


    fun calculateTotalPawnScore(color: PieceColor): Double {
        var totalPawnScore = 0.0

        val pawnChains =
            if (color == PieceColor.WHITE) board.getPawnChains(PieceColor.WHITE) else board.getPawnChains(
                PieceColor.BLACK
            )

        for (chain in pawnChains) {
            totalPawnScore += calculatePawnChainScore(chain, color)
            totalPawnScore += calculateDoubledPawnScore(chain, color)
            totalPawnScore += calculateTripledPawnScore(chain, color)
            totalPawnScore += calculateBackwardPawnScore(chain, color)
            totalPawnScore += calculateSentryPawnScore(chain, color)
            totalPawnScore += calculatePassedPawnScore(chain, color)
        }

        println("Total Pawn Score: $totalPawnScore for color $color")
        return totalPawnScore
    }
}