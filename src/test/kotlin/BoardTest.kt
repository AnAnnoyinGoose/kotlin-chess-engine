import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

class BoardTest {

    @Test
    fun getPiecesInFile() {
        val board = Board("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
        assertEquals(4, board.getPiecesInFile(Files.B).size)
    }

    @Test
    fun getPiecesInRank() {
        val board = Board("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
        assertEquals(8, board.getPiecesInRank(Ranks.RANK_1).size)
        assertEquals(0, board.getPiecesInRank(Ranks.RANK_5).size)
    }
}