import org.junit.jupiter.api.Assertions.*
import kotlin.test.Test

class FENValidatorTest {
    @Test
    fun isValid_ValidFEN() {
        assertTrue(FENValidator("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1").isValid())
    }

    @Test
    fun isValid_InvalidLength() {
        assertFalse(FENValidator("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq -").isValid())
    }

    @Test
    fun isValid_InvalidSideToMove() {
        assertFalse(FENValidator("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR x KQkq - 0 1").isValid())
    }

    @Test
    fun isValid_InvalidCastlingAbility() {
        assertFalse(FENValidator("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkqyx - 0 1").isValid())
    }

    @Test
    fun isValid_InvalidEnPassantTarget() {
        assertFalse(FENValidator("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq x3 - 0 1").isValid())
    }

    @Test
    fun isValid_InvalidHalfMoveClock() {
        assertFalse(FENValidator("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - abc 1").isValid())
    }

    @Test
    fun isValid_InvalidFullMoveCounter() {
        assertFalse(FENValidator("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 xyz").isValid())
    }

    @Test
    fun isValid_InvalidRank() {
        assertFalse(FENValidator("rnbqkbnr/pppppppX/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1").isValid())
    }

    @Test
    fun isValid_InvalidSideToMoveFormat() {
        assertFalse(FENValidator("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR invalid KQkq - 0 1").isValid())
    }

    @Test
    fun isValid_InvalidCastlingAbilityFormat() {
        assertFalse(FENValidator("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkqx - 0 1").isValid())
    }
}